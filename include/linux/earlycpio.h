#ifndef _LINUX_EARLYCPIO_H
#define _LINUX_EARLYCPIO_H

#include <linux/types.h>

struct cpio_data {
	void *data;
	size_t size;
};

struct cpio_data find_cpio_data(const char *name, const void *data, size_t len);

#endif /* _LINUX_EARLYCPIO_H */
