/* ----------------------------------------------------------------------- *
 *
 *   Copyright 2012 Intel Corporation; author H. Peter Anvin
 *
 *   This file is part of the Linux kernel, and is made available
 *   under the terms of the GNU General Public License version 2, as
 *   published by the Free Software Foundation.
 *
 *   This program is distributed in the hope it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 * ----------------------------------------------------------------------- */

/*
 * earlycpio.c
 *
 * Find a specific cpio member; must precede any compressed content.
 * This is used to locate data items in the initramfs used by the
 * kernel itself during early boot (before the main initramfs is
 * decompressed.)  It is the responsibility of the initramfs creator
 * to ensure that these items are uncompressed at the head of the
 * blob.  Depending on the boot loader or package tool that may be a
 * separate file or part of the same file.
 *
 * For some architectures, e.g. i386, this file must compile to have
 * no relocations and no library dependencies, so it can be called from
 * a nonstandard environment.  Therefore some normal library functions
 * are inlined in this file.
 */

#include <linux/earlycpio.h>
#include <linux/kernel.h>

enum cpio_fields {
	C_MAGIC,
	C_INO,
	C_MODE,
	C_UID,
	C_GID,
	C_NLINK,
	C_MTIME,
	C_FILESIZE,
	C_MAJ,
	C_MIN,
	C_RMAJ,
	C_RMIN,
	C_NAMESIZE,
	C_CHKSUM,
	C_NFIELDS
};

#ifdef CONFIG_X86
static inline size_t strlen(const char *name)
{
	size_t n = -1;

	asm("repne; scasb"
	    : "+D" (name), "+c" (n)
	    : "a" (0));

	return -2 - n;
}

static inline int memcmp(const void *p1, const void *p2, size_t n)
{
	unsigned char rv;

	asm("repe; cmpsb; setne %0"
	    : "=r" (rv), "+S" (p1), "+D" (p2), "+c" (n));

	return rv;
}
#else
static inline size_t strlen(const char *name)
{
	size_t n = 0;

	while (*name++)
		n++;

	return n;
}

static inline int memcmp(const void *p1, const void *p2, size_t n)
{
	const unsigned char *u1 = p1;
	const unsigned char *u2 = p2;
	int d;

	while (n--) {
		d = *u2++ - *u1++;
		if (d)
			return d;
	}
	return 0;
}
#endif

struct cpio_data __cpuinit find_cpio_data(const char *name,
					  const void *data, size_t len)
{
	const size_t cpio_header_len = 8*C_NFIELDS - 2;
	struct cpio_data cd = { NULL, 0 };
	const char *p, *dptr, *nptr;
	unsigned int ch[C_NFIELDS], *chp, v;
	unsigned char c, x;
	size_t mynamesize = strlen(name) + 1;
	int i, j;

	p = data;

	while (len > cpio_header_len) {
		if (!*p) {
			/* All cpio headers need to be 4-byte aligned */
			p += 4;
			len -= 4;
			continue;
		}

		j = 6;		/* The magic field is only 6 characters */
		chp = ch;
		for (i = C_NFIELDS; i; i--) {
			v = 0;
			while (j--) {
				v <<= 4;
				c = *p++;

				x = c - '0';
				if (x < 10) {
					v += x;
					continue;
				}

				x = (c | 0x20) - 'a';
				if (x < 6) {
					v += x + 10;
					continue;
				}

				goto quit; /* Invalid hexadecimal */
			}
			*chp++ = v;
			j = 8;	/* All other fields are 8 characters */
		}

		if ((ch[C_MAGIC] - 0x070701) > 1)
			goto quit; /* Invalid magic */

		len -= cpio_header_len;

		dptr = PTR_ALIGN(p + ch[C_NAMESIZE], 4);
		nptr = PTR_ALIGN(dptr + ch[C_FILESIZE], 4);

		if (nptr > p + len || dptr < p || nptr < dptr)
			goto quit; /* Buffer overrun */

		if ((ch[C_MODE] & 0170000) == 0100000 &&
		    ch[C_NAMESIZE] == mynamesize &&
		    !memcmp(p, name, mynamesize)) {
			cd.data = (void *)dptr;
			cd.size = ch[C_FILESIZE];
			return cd; /* Found it! */
		}

		len -= (nptr - p);
		p = nptr;
	}

quit:
	return cd;
}
